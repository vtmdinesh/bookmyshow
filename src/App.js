import './App.css';
import Header from './components/Header';
import { connect } from 'react-redux';
import getMoviesList from './components/actions';
import getPopularMoviesList from './components/actionPopularMovies';
import MoviesHome from './components/MoviesHome';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import React, { Component } from 'react'
import MovieDetails from './components/MovieDetails';
import AllRecommendedMovies from './components/AllRecommendedMovies';
import AllPopularMovies from './components/AllPopularMovies';
import TheaterPage from './components/TheaterPage';
import TicketPage from './components/TicketPage';
import TheatreSeatingLayout from './components/TheatreSeatingLayout';
import PaymentPage from './components/PaymentPage';
import SuccessPage from './components/SuccessPage';
class App extends Component {

  componentDidMount() {
    // console.log('didmount')
    this.props.getMoviesList();
    this.props.getPopularMoviesList()


  }
  render() {
    return (
      <BrowserRouter>
        <Header />
        <Switch>
          <Route exact path="/" component={MoviesHome} />
          <Route exact path={`/movie/:id`} component={MovieDetails} />
          <Route exact path="/recommandedMovies" component={AllRecommendedMovies} />
          <Route exact path="/popularMovies" component={AllPopularMovies} />
          <Route exact path="/theaterpage" component={TheaterPage} />
          <Route exact path="/ticketpage" component={TicketPage} />
          <Route exact path="/layout" component={TheatreSeatingLayout} />
          <Route exact path="/paymentpage" component={PaymentPage} />
          <Route exact path="/successpage" component={SuccessPage} />
        </Switch>
      </BrowserRouter>
    )
  }
}




const mapStateToProps = (state) => ({
  local_variable: state

})





export default connect(mapStateToProps, { getMoviesList, getPopularMoviesList })(App);

import { GET_EMAIL } from "./action-types"
import validator from "validator"


const getEmailAndValidate = (event) => dispatch => {

    let email = event.target.value

    let isValidEmail = validator.isEmail(email)
    console.log(isValidEmail)

    dispatch({
        type: GET_EMAIL,
        payload: { isValidEmail: isValidEmail, email: email }
    })
};


export default getEmailAndValidate;
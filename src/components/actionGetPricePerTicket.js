import { GET_PRICE_PER_TICKET } from "./action-types"


const getPricePerTicket = (price) => dispatch => {

    dispatch({
        type: GET_PRICE_PER_TICKET,
        payload: parseInt(price)
    })
};


export default getPricePerTicket;
import { GET_MOBILE_NUMBER } from "./action-types"
import validator from "validator"


const getMobileNumAndValidate = (event) => dispatch => {

    let mobNumber = event.target.value

    let isValid = validator.isMobilePhone(mobNumber, 'en-IN')
    console.log(isValid)

    dispatch({
        type: GET_MOBILE_NUMBER,
        payload: { isValid: isValid, mobile: mobNumber }
    })
};


export default getMobileNumAndValidate;
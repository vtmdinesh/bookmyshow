import { FETCH_MOVIE_DETAIL } from "./action-types"
import axios from "axios"

const getMovieDetail = (id) => dispatch => {

    axios.get(`https://api.themoviedb.org/3/movie/${id}?api_key=c88885ed1df6c2b1eec648f6a4cc3707&language=en-US`)
        .then(res => res.data)
        .then(data => dispatch({
            type: FETCH_MOVIE_DETAIL,
            payload: data
        }))
};


export default getMovieDetail;
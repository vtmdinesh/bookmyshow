import { GET_SEAT_NUMBERS } from "./action-types"



const getSeatNumbers = (array) => dispatch => {

    dispatch({
        type: GET_SEAT_NUMBERS,
        payload: array
    })
};


export default getSeatNumbers;
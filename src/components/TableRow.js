import React, { Component } from 'react'
import { connect } from 'react-redux'
import getSeatNumbers from './actionGetSeatNumbers'



class TableRow extends Component {

    selectSeat = (event) => {
        let id = event.target.id
        let seat_number = this.props.local_variable.seat_number
        let numberOfSeats = this.props.local_variable.numberOfSeats

        if (numberOfSeats > seat_number.length) {
            event.target.classList.toggle("selected")
            if (seat_number.length >= 0) {
                let newArray = seat_number.filter((eachId) => eachId !== id)

                if (newArray.length === seat_number.length && newArray.length < numberOfSeats) {
                    console.log("in if")
                    this.props.getSeatNumbers([...newArray, id])
                }
                else {
                    console.log("in else")
                    this.props.getSeatNumbers([...newArray])
                }
            }
        }
        else if (numberOfSeats == seat_number.length) {

            let newArray = seat_number.filter((eachId) => eachId !== id)
            if (newArray.length !== seat_number.length) {
                event.target.classList.toggle("selected")
                this.props.getSeatNumbers([...newArray])
            }
        }
    }


    render() {
        console.log(this.props.local_variable.seat_number)
        let rowId = this.props.rowId

        return (
            <div className='col overflow-smooth d-flex justify-content-center'>
                <td colSpan="2"><div className='text-secondary fs-6'>{rowId}</div></td>
                <td colSpan="2"><div className='empty-box'></div></td>
                <td colSpan="2"><div className='empty-box'></div></td>
                <td colSpan="2"><div className='box text-center' id={`${rowId}-1`} onClick={this.selectSeat} value="1">1</div></td>
                <td colSpan="2"><div className='box text-center ' id={`${rowId}-2`} value="1" onClick={this.selectSeat} >2</div></td>
                <td colSpan="2"><div className='box text-center ' id={`${rowId}-3`} value="1" onClick={this.selectSeat} >3</div></td>
                <td colSpan="2"><div className='box text-center ' id={`${rowId}-4`} value="1" onClick={this.selectSeat} >4</div></td>
                <td colSpan="2"><div className='box text-center ' id={`${rowId}-5`} value="1" onClick={this.selectSeat} >5</div></td>
                <td colSpan="2"><div className='box text-center ' id={`${rowId}-6`} value="1" onClick={this.selectSeat}>6</div></td>
                <td colSpan="2"><div className='box text-center ' id={`${rowId}-7`} value="1" onClick={this.selectSeat}>7</div></td>
                <td colSpan="2"><div className='box text-center ' id={`${rowId}-8`} value="1" onClick={this.selectSeat}>8</div></td>
                <td colSpan="2"><div className='box text-center ' id={`${rowId}-9`} value="1" onClick={this.selectSeat}>9</div></td>
                <td colSpan="2"><div className='box text-center ' id={`${rowId}-10`} value="1" onClick={this.selectSeat}>10</div></td>
                <td colSpan="2"><div className='box text-center ' id={`${rowId}-11`} value="1" onClick={this.selectSeat}>11</div></td>
                <td colSpan="2"><div className='box text-center ' id={`${rowId}-12`} value="1" onClick={this.selectSeat}>12</div></td>
                <td colSpan="2"><div className='box text-center ' id={`${rowId}-13`} value="1" onClick={this.selectSeat}>13</div></td>
                <td colSpan="2"><div className='empty-box'></div></td>
                <td colSpan="2"><div className='empty-box'></div></td>
                <td colSpan="2"><div className='box text-center ' id={`${rowId}-14`} value="1" onClick={this.selectSeat}>14</div></td>
                <td colSpan="2"><div className='box text-center ' id={`${rowId}-15`} value="1" onClick={this.selectSeat}>15</div></td>
                <td colSpan="2"><div className='box text-center ' id={`${rowId}-16`} value="1" onClick={this.selectSeat}>16</div></td>
                <td colSpan="2"><div className='box text-center' id={`${rowId}-17`} value="1" onClick={this.selectSeat}>17</div></td>
                <td colSpan="2"><div className='box text-center' id={`${rowId}-18`} value="1" onClick={this.selectSeat}>18</div></td>
                <td colSpan="2"><div className='box text-center' id={`${rowId}-19`} value="1" onClick={this.selectSeat}>19</div></td>
                <td colSpan="2"><div className='box text-center ' id={`${rowId}-20`} value="1" onClick={this.selectSeat}>20</div></td>
                <td colSpan="2"><div className='box text-center' id={`${rowId}-21`} value="1" onClick={this.selectSeat}>21</div></td>
            </div>
        )
    }
}






let mapStateToProps = (state) => ({
    local_variable: state
})

export default connect(mapStateToProps, { getSeatNumbers })(TableRow)
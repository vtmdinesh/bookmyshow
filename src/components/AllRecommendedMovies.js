import React, { Component } from 'react'
import MovieTile from './MovieTile'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

class AllRecommendedMovies extends Component {
    render() {
        let moviesList = this.props.local_variable.recommendMovies[0]
        console.log(moviesList)
        return (
            <div className="d-flex flex-column justify-content-center align-items-center">
                <h2 className='mt-5'>All Recommanded Movies</h2>
                <Link className=' align-self-end me-5' to="/"> <button className='sign-in-bg btn me-auto  mt-5 mb-5'> Back To home</button></Link>
                <div className='container'>
                    <div className='row'>

                        {((moviesList) ? (moviesList.map(eachMovie => {
                            return <MovieTile movieData={eachMovie} key={eachMovie.id}
                            />
                        })) : null
                        )}

                    </div>
                </div>


            </div >



        )
    }
}



const mapStateToProps = (state) => ({
    local_variable: state

})

export default connect(mapStateToProps)(AllRecommendedMovies)



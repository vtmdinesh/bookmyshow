import { FETCH_MOVIE_DETAIL, FETCH_SIMILAR_MOVIES } from "./action-types"
import axios from "axios"


export const getId = (event) => (dispatch) => {
    let id = event.target.id
    console.log(id)
    console.log("in action")
    axios.get(`https://api.themoviedb.org/3/movie/${id}?api_key=c88885ed1df6c2b1eec648f6a4cc3707&language=en-US`)
        .then(res => res.data)
        .then(data => dispatch({
            type: FETCH_MOVIE_DETAIL,
            payload: data
        })).then(axios.get(`https://api.themoviedb.org/3/movie/${id}/similar?api_key=c88885ed1df6c2b1eec648f6a4cc3707&language=en-US&page=1`)
            .then(res => res.data)
            .then(data => dispatch({
                type: FETCH_SIMILAR_MOVIES,
                payload: data
            })))


}
import { FETCH_SIMILAR_MOVIES } from "./action-types"
import axios from "axios"

const getSimilarMovieDetail = (id) => dispatch => {

    axios.get(`https://api.themoviedb.org/3/movie/${id}/similar?api_key=c88885ed1df6c2b1eec648f6a4cc3707&language=en-US&page=1`)
        .then(res => res.data)
        .then(data => dispatch({
            type: FETCH_SIMILAR_MOVIES,
            payload: data
        }))
};


export default getSimilarMovieDetail;
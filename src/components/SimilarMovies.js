import React, { Component } from 'react'
import { connect } from 'react-redux'
import MovieTile from './MovieTile'
import Spinner from './Spinner'


class SimilarMovies extends Component {

    render() {
        let moviesList = this.props.local_variable.similarMovies
        console.log(moviesList)

        return (
            <div style={{ "flexBasis": "300px" }} className='overflow-auto d-flex flex-row w-100 tile'>

                {
                    (moviesList == []) ? <Spinner /> : null
                }

                {
                    (moviesList !== undefined && moviesList !== []) ? (<div id="carouselExampleControls-5 mt-2" class="carousel" data-bs-ride="carousel">
                        <div class="carousel-inner transition">
                            <div class="carousel-item active">
                                {
                                    <div className='d-flex'>
                                        <MovieTile movieData={moviesList.results[0]} key={moviesList.results[0].id} />
                                        <MovieTile movieData={moviesList.results[1]} key={moviesList.results[1].id} />
                                        <MovieTile movieData={moviesList.results[2]} key={moviesList.results[2].id} />
                                        <MovieTile movieData={moviesList.results[3]} key={moviesList.results[3].id} />
                                        <MovieTile movieData={moviesList.results[4]} key={moviesList.results[4].id} />
                                        <MovieTile movieData={moviesList.results[5]} key={moviesList.results[5].id} />

                                    </div>
                                }
                            </div>
                            <div class="carousel-item">
                                <div className='d-flex'>

                                    {
                                        // <MovieTile movieData={moviesList.results[6]} key={moviesList.results[6].id} />
                                        // <MovieTile movieData={moviesList.results[7]} key={moviesList.results[7].id} />
                                        // <MovieTile movieData={moviesList.results[8]} key={moviesList.results[8].id} />
                                        // <MovieTile movieData={moviesList.results[9]} key={moviesList.results[9].id} />
                                        // <MovieTile movieData={moviesList.results[10]} key={moviesList.results[10].id} />
                                        // <MovieTile movieData={moviesList.results[11]} key={moviesList.results[11].id} />
                                    }
                                </div>
                            </div>
                            {
                                // <div class="carousel-item">
                                //     <div className='d-flex'>
                                //         <MovieTile movieData={moviesList.results[12]} key={moviesList.results[12].id} />
                                //         <MovieTile movieData={moviesList.results[13]} key={moviesList.results[13].id} />
                                //         <MovieTile movieData={moviesList.results[14]} key={moviesList.results[14].id} />
                                //         <MovieTile movieData={moviesList.results[15]} key={moviesList.results[15].id} />

                                //     </div>
                                // </div>
                                // <div class="carousel-item">
                                //     <div className='d-flex'>
                                //         <MovieTile movieData={moviesList.results[16]} key={moviesList.results[16].id} />
                                //         <MovieTile movieData={moviesList.results[17]} key={moviesList.results[17].id} />
                                //         <MovieTile movieData={moviesList.results[18]} key={moviesList.results[18].id} />
                                //         <MovieTile movieData={moviesList.results[19]} key={moviesList.results[19].id} />

                                //     </div>
                                // </div>
                            }
                        </div>
                        <button className="carousel-control-prev bt" type="button" data-bs-target="#carouselExampleControls-5" data-bs-slide="prev">
                            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span className="visually-hidden">Previous</span>
                        </button>
                        <button className="carousel-control-next bt" type="button" data-bs-target="#carouselExampleControls-5" data-bs-slide="next">
                            <span className="carousel-control-next-icon" aria-hidden="true"></span>
                            <span className="visually-hidden">Next</span>
                        </button>
                    </div>) : null}




            </div>
        )
    }
}




const mapStateToProps = (state) => ({
    local_variable: state

})

export default connect(mapStateToProps)(SimilarMovies)

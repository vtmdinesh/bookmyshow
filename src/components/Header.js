import "../App.css"
import React, { Component } from 'react'
import { Link } from "react-router-dom"
import { connect } from "react-redux"
import getMobileNumAndValidate from "./actionGetMobileNumberAndValidate"
import getEmailAndValidate from "./actionGetEmailAndValidate"


class Header extends Component {
    constructor(props) {
        super(props)

        this.state = {
            numberInput: "",
            emailInput: "",
            isSignedIn: false
        }
    }
    render() {

        let mobNumber = this.props.local_variable.mobNumberDetails.mobile
        let isValid = this.props.local_variable.mobNumberDetails.isValid
        // console.log(this.props.local_variable)
        let email = this.props.local_variable.emailDetails.email
        let isValidEmail = this.props.local_variable.emailDetails.isValidEmail

        let buttonText = (isValid || isValidEmail) ? "Sign out" : "Sign in"
        return (
            <div>
                <nav className="navbar navbar-expand-lg nav-bg ">

                    <div className="container-fluid w-100 align-items-center d-flex flex-row justify-content-around">
                        <div className="d-flex flex-row w-50 align-items-center">
                            <Link to="/" className="navbar-brand" ><img className="logo" src="./images/logo.png" alt="logo" /> </Link>

                            <div className="d-flex flex-row h-75 w-75 ">
                                <div className="bg-white icon-container d-flex justify-content-center align-items-center border-0 p-1 ">
                                    <i class="fa-solid fa-magnifying-glass m-auto "></i>
                                </div>
                                <input className="border-0 me-2 w-100" type="search" placeholder="Search For Movies, Events,Plays, Sports and Activities " aria-label="Search" />
                            </div>
                        </div>
                        <div className="d-flex w-25 flex-row justify-content-between align-items-center">
                            <button className="navbar-toggler ms-2" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span className="navbar-toggler-icon"></span>
                            </button>
                            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul className="navbar-nav me-auto mb-2 ms-2 mb-lg-0">

                                    <li className="nav-item dropdown ">
                                        <a className="nav-link dropdown-toggle header-text" href="null" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                            Bengaluru
                                        </a>
                                        <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <div className="d-flex flex-column w-100">

                                                <li><a className="dropdown-item" href="null">
                                                    <div className="d-flex flex-column justify-content-center align-items-center"> <img className="city-logo" src="//in.bmscdn.com/m6/images/common-modules/regions/bang.png" alt="chennai" />
                                                        <p>Bengaluru</p></div></a></li>
                                                <li><hr className="dropdown-divider" /></li>
                                                <li><a className="dropdown-item" href="null">
                                                    <div className="d-flex flex-column justify-content-center align-items-center"> <img className="city-logo" src="//in.bmscdn.com/m6/images/common-modules/regions/chen.png" alt="chennai" />
                                                        <p>Chennai</p></div></a></li>
                                                <li><hr className="dropdown-divider" /></li>
                                                <li><a className="dropdown-item" href="null">
                                                    <div className="d-flex flex-column justify-content-center align-items-center"> <img className="city-logo" src="//in.bmscdn.com/m6/images/common-modules/regions/mumbai.png" alt="mumbai" />
                                                        <p>Mumbai</p></div></a></li>
                                                <li><hr className="dropdown-divider" /></li>
                                                <li><a className="dropdown-item" href="null">
                                                    <div className="d-flex flex-column justify-content-center align-items-center"> <img className="city-logo" src="//in.bmscdn.com/m6/images/common-modules/regions/ncr.png" alt="mumbai" />
                                                        <p>NCR</p></div></a></li> </div>
                                        </ul>
                                    </li>
                                    <button type="button" class="sign-in-bg btn border-0 ps-4 pe-4 " data-bs-toggle="modal" data-bs-target="#exampleModal">{buttonText}
                                    </button>

                                    <div class="modal fade model-1 " id="exampleModal" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                                            <div class="modal-content">
                                                <div class="modal-header ">
                                                    <h5 class="modal-title ms-auto me-auto" id="exampleModalLabel">Get Started</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body border-0 d-flex flex-column align-items-center">
                                                    <button className="btn button-header rounded-3 mt-4  h-100 p-3"><img src="./google.png" className="me-5" alt="logo" />Continue with Google</button>
                                                    <button className="btn mt-4 button-header rounded-3 h-100 p-3" data-bs-toggle="modal" data-bs-target="#staticBackdrop2" type="button"><img src="./email.png" className="me-5 " alt="logo" />Continue with Email</button>

                                                    <div class="modal fade modal-2" id="staticBackdrop2" data-bs-backdrop="static" data-bs-keyboard="false" tabndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                                                            <div class="modal-content p-4">

                                                                <h5 class="modal-title" id="staticBackdropLabel" data-bs-dismiss="modal" aria-label="Close"><i class="fa-solid fa-less-than fs-6 me-5 ms-2"></i></h5>


                                                                <div class="modal-body">
                                                                    <h3 className="mt-3">Login with Email</h3>
                                                                    <label htmlFor="emailInput" className="mt-2 mb-2">Email</label>
                                                                    <br />
                                                                    <input type="text" onChange={this.props.getEmailAndValidate} value={email} placeHolder="Continue with Email" className="w-100" id="emailInput"></input>
                                                                    {!isValidEmail && email ? <p className="text-danger"> Please Enter Valid Email !!!!</p> : null}
                                                                    <Link to="/" ><button disabled={!isValidEmail} className="continue-btn  nested-btn p-2 " data-bs-dismiss="modal" aria-label="Close">Continue</button></Link>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>





                                                    <button className="btn mt-4 button-header rounded-3 h-100 p-3"><img src="./apple.png" className="me-5" alt="logo" />      Continue with Apple</button>
                                                    <h3 className="text-center text-secondary fs-6 fw-light mt-3"> OR</h3>
                                                    <div className="text-center">
                                                        <label htmlFor="mob-input"><img src="./flag.png" alt="flag" />+91 </label>
                                                        <input onChange={this.props.getMobileNumAndValidate} type="text" value={mobNumber} id="mob-input" placeHolder="Continue with mobile number" className="border border-bottom"></input>

                                                        {!isValid && mobNumber ? <p className="text-danger"> Please Enter Valid Mobile Number !!!!</p> : null}

                                                    </div>

                                                    {(mobNumber) ? <Link to="/" ><button disabled={!isValid} className="continue-btn p-2 mt-5" data-bs-dismiss="modal" aria-label="Close">Continue</button></Link> : (
                                                        <p className="terms text-center mt-5 pb-3">I agree to the <u>Terms & Conditions </u> &  <u>Privacy Policy</u></p>)
                                                    }


                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <i class="fa-solid fa-bars icon ms-2"></i>

                                </ul>
                            </div>
                        </div>
                    </div>
                </nav>

                <div className="d-none d-md-block w-100 d-md-flex justify-content-around bg-second-header p-2" >
                    <ul className="list-group d-flex flex-row list-style-none">
                        <li className="ms-3"> Movies</li>
                        <li className="ms-3">Stream<sup className="text-right sup-color">New</sup></li>
                        <li className="ms-3">Events</li>
                        <li className="ms-3">Plays</li>
                        <li className="ms-3">Sports</li>
                        <li className="ms-3">Activities</li>
                        <li className="ms-3">Buzz</li>
                    </ul>
                    <ul className="list-group d-flex flex-row list-style-none">
                        <li className="ms-3">List Your Shows <sup className="text-right sup-color">New</sup></li>
                        <li className="ms-3">Corporates</li>
                        <li className="ms-3">Offers</li>
                        <li className="ms-3">Gift Cards</li>
                    </ul>
                </div>


            </div>
        )
    }
}

let mapStateToProps = (state) => ({
    local_variable: state
})


export default connect(mapStateToProps, { getMobileNumAndValidate, getEmailAndValidate })(Header)
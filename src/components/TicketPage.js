import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import getNumberOfSeats from './actionGetNumberOfSeats'
import getPricePerTicket from './actionGetPricePerTicket'

class TicketPage extends Component {
    constructor(props) {
        super(props)

        this.state = {
            isSeatsChosen: false,
            numberOfSeats: 1,
            ticketPrice: 120
        }
    }

    selectNumberOfTickets = (event) => {
        let numberOfSeats = event.target.value

        this.setState({ numberOfSeats: numberOfSeats })
    }

    getImage = (numberOfSeats) => {
        switch (parseInt(numberOfSeats)) {
            case 1:

                return <img src="./cycle.png" alt="vehicle" />

            case 2:

                return <img src="./scooter.png" alt="vehicle" />

            case 3:

                return <img src="./auto.png" alt="vehicle" />

            case 4:
                return <img src="./nano_car.png" alt="vehicle" />

            case 5:
                return <img src="./sedan_car.png" alt="vehicle" />
            case 6:
                return <img src="./sedan_car.png" alt="vehicle" />


            case 7:
                return <img src="./sedan_car.png" alt="vehicle" />
            case 8:
                return <img src="./mini_van.png" alt="vehicle" />
            case 9:
                return <img src="./mini_van.png" alt="vehicle" />
            case 10:
                return <img src="./mini_van.png" alt="vehicle" />
            default:
                break;
        }

    }


    getTicketPrice = (event) => {
        let price = parseInt(event.target.value)
        this.setState({ ticketPrice: price })
    }


    render() {
        let { isSeatsChosen, numberOfSeats } = this.state
        // console.log(this.getImage(numberOfSeats))
        // console.log(this.props.local_variable)
        let ticketPrice = this.state.ticketPrice
        this.props.getPricePerTicket(ticketPrice)
        let numbersArray = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        // console.log(this.state.numberOfSeats)

        return (
            <div className='ticket-modal d-flex flex-column justify-content-center align-items-center'>

                {
                    (isSeatsChosen) ? null : (
                        <div className='d-flex bg-white tkt-container  rounded-3 flex-column justify-content-center align-items-center'>
                            <h3>How Many Seats?</h3>
                            {this.getImage(numberOfSeats)}
                            <div>
                                {
                                    numbersArray.map(number => {
                                        let selectedClass = (numberOfSeats == parseInt(number)) ? "selected-number" : null


                                        return (
                                            <button class={`btn ticket-btn rounded-circle p-1 ms-3 ${selectedClass}`} value={`${number}`} id={`${number}`} onClick={this.selectNumberOfTickets}  >{number}</button>)

                                    })


                                }
                            </div>
                            <hr className='text-dark' />
                            <div className='d-flex container flex-row'>
                                <div className='row'>
                                    <div className='d-flex col border border-dark col-md-3 flex-column justify-content-center align-items-center ms-4 m-1 bg-white border-0' value="120" type="button" onClick={this.getTicketPrice}>
                                        <button value="120" className="text-secondary bg-white border-0 fs-6">CLASSIC</button>
                                        <button value="120" className='text-dark fw-bold bg-white border-0 fs-6'> Rs. 120.00</button>
                                        <button value="120" className='text-success bg-white border-0 avl'>Available </button>
                                    </div>
                                    <div className='d-flex col  border border-dark col-md-3 flex-column  m-1 justify-content-center align-items-center ms-4 bg-white border-0 ' value="200" onClick={this.getTicketPrice}>
                                        <button value="200" className="text-secondary bg-white border-0  fs-6">PRIME</button>
                                        <button value="200" className='text-dark fw-bold bg-white border-0  fs-6'> Rs.200.00</button>
                                        <button value="200" className='text-success bg-white border-0 avl'>Available </button>
                                    </div>
                                    <div className='d-flex m-1 col col-md-3 flex-column bg-white border-0 justify-content-center align-items-center ms-4' value="340" onClick={this.getTicketPrice}>
                                        <button value="340" className="text-secondary bg-white border-0 fs-6">VIP</button>
                                        <button value="340" className='text-dark fw-bold bg-white border-0 fs-6'>Rs.340.00 </button>
                                        <button value="340" className='text-success bg-white border-0 avl'>Available </button>
                                    </div>
                                    <div className='d-flex  m-1 col col-md-3 flex-column bg-white border-0 justify-content-center align-items-center ms-4' value="500" onClick={this.getTicketPrice}>
                                        <button value="500" className="text-secondary bg-white border-0 fs-6" >COUPLE</button>
                                        <button value="500" className='text-dark fw-bold bg-white border-0 fs-6'> Rs. 500.00</button>
                                        <button value="500" className='text-success bg-white border-0 avl'>Available </button>
                                    </div>
                                </div>
                            </div>
                            <Link to="/layout" className='sign-in-bg fs-6 w-75 mt-4 p-2 text-center rounded-3 border-0'> <button className="sign-in-bg border-0 w-100 h-100 " value={this.state.numberOfSeats} onClick={this.props.getNumberOfSeats} >Select Seats</button></Link>

                        </div>
                    )
                }
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    local_variable: state

})

export default connect(mapStateToProps, { getNumberOfSeats, getPricePerTicket })(TicketPage) 
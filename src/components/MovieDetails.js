import React, { Component } from 'react'
import getMovieDetail from './actionGetMovieDetails'
import { connect } from 'react-redux'
import getSimilarMovieDetail from './actionGetSimilarMovies'
import { Link } from "react-router-dom"
// import SimilarMovies from './SimilarMovies'
// import Spinner from './Spinner'


class MovieDetails extends Component {


    render() {
        let movieData = this.props.local_variable.movieDetail
        console.log(movieData)
        return (
            <div className=''>

                {
                    (movieData) ? <div className='w-100'>
                        <div className='bg-dark vh-25 w-100 d-flex flex-column flex-md-row justify-content-center align-items-center justify-content-md-around pt-4 pb-4'>
                            <div className='d-flex flex-column  flex-md-row justify-self-end w-50 h-100'>
                                <img className="w-50  h-100 rounded-3" alt="poster" src={`https://image.tmdb.org/t/p/original/${movieData.poster_path}`} />
                                <div className='d-flex ms-4 flex-column'>
                                    <h5 className='text-white mt-3'>{movieData.title}</h5>
                                    <div className='d-flex flex-md-row bg-dark w-100 text-white radius-2 justify-content-start align-items-center'>
                                        <i class="fa-solid fa-thumbs-up text-success fs-1"></i>
                                        <p className='align-self-center ms-2 fs-4 pt-3'>{`${movieData.vote_average * 10}%`}</p>
                                        <p className='ms-2 mt-1 fs-5 pt-2' > {`${movieData.vote_count}K votes`}</p>
                                    </div>
                                    <div className='ratings-container w-100 p-1 mb-3 d-flex  flex-column flex-md-row align-items-center justify-content-between'>
                                        <div>
                                            <h4>Add your Ratings & review</h4>
                                            <p>Your Ratings Matter</p>
                                        </div>
                                        <button className='btn btn-movie'>Rate now</button>
                                    </div>
                                    <div className='d-flex '>
                                        <p className='btn btn-movie w-50'>2D</p>

                                        <p className='btn btn-movie w-50 ms-2'>English</p>
                                    </div>
                                    <p className='text-white'>Released On : {movieData.release_date}</p>
                                    <p className='text-white'>{movieData.runtime} mins</p>
                                    <Link to="/theaterpage" ><button className='sign-in-bg btn-book '>Book tickets</button>
                                    </Link>
                                </div>
                            </div>
                            <button className='btn btn-share mt-4'> <i className="fa-solid fa-share-nodes"></i> Share</button>
                        </div>
                        <div className='ms-5'>
                            <h3>About the movie</h3>
                            <p>{movieData.overview}</p>
                            <hr className='text-secondary' />
                            <h3>Applicable offers</h3>
                            <div className='offers-container'>
                                <p className='fw-bold'> <i class="fa-regular fa-bars"></i>  STREAM THE RRREVLOUTION </p>
                                <p>Starting @ Rs.179/- only</p>
                            </div>

                            <hr className='text-secondary' />
                            <h3 className='fs-3'>Cast</h3>
                            <div className='d-flex container-fluid flex-row justify-content-start'>
                                <div className='row'>
                                    <div className='d-flex col flex-column justify-content-center align-items-center '>
                                        <div className="rounded-circle cast bg-secondary"></div>
                                        <p>Name</p>
                                        <p>Character Name</p>
                                    </div>
                                    <div className='d-flex col flex-column justify-content-center align-items-center '>
                                        <div className="rounded-circle cast bg-secondary"></div>
                                        <p>Name</p>
                                        <p>Character Name</p>
                                    </div>
                                    <div className='d-flex col flex-column justify-content-center align-items-center'>
                                        <div className="rounded-circle cast bg-secondary"></div>
                                        <p>Name</p>
                                        <p>Character Name</p>
                                    </div>
                                    <div className='d-flex col flex-column justify-content-center align-items-center'>
                                        <div className="rounded-circle cast bg-secondary"></div>
                                        <p>Name</p>
                                        <p>Character Name</p>
                                    </div>
                                </div>
                            </div>
                            <hr className='text-secondary' />
                            <h3 className='fs-3'>Crew</h3>
                            <div className='d-flex container-fluid flex-row justify-content-start'>

                                <div className='row'>
                                    <div className='d-flex col flex-column justify-content-center align-items-center'>
                                        <div className="rounded-circle cast bg-secondary"></div>
                                        <p>Name</p>
                                        <p>Role</p>
                                    </div>
                                    <div className='d-flex col flex-column justify-content-center align-items-center'>
                                        <div className="rounded-circle cast bg-secondary"></div>
                                        <p>Name</p>
                                        <p>Role</p>
                                    </div>
                                    <div className='d-flex col  flex-column justify-content-center align-items-center'>
                                        <div className="rounded-circle cast bg-secondary"></div>
                                        <p>Name</p>
                                        <p>Role</p>
                                    </div>
                                    <div className='d-flex col flex-column justify-content-center align-items-center'>
                                        <div className="rounded-circle cast bg-secondary"></div>
                                        <p>Name</p>
                                        <p>Role</p>
                                    </div>
                                </div>
                            </div>
                            <hr className='text-secondary' />
                            {
                                //     <h3 className='fs-3'>You might like</h3>
                                // <SimilarMovies />
                            }
                        </div>
                    </div> : null
                }

            </div>
        )
    }
}


const mapStateToProps = (state) => ({
    local_variable: state

})


export default connect(mapStateToProps, { getMovieDetail, getSimilarMovieDetail })(MovieDetails);



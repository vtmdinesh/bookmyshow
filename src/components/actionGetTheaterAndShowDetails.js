import { GET_THEATER_AND_SHOW_DETAILS } from "./action-types"


const getTheaterAndShowDetail = (event) => dispatch => {

    dispatch({
        type: GET_THEATER_AND_SHOW_DETAILS,
        payload: { theaterName: event.target.name, showTime: event.target.value }
    })
};


export default getTheaterAndShowDetail;
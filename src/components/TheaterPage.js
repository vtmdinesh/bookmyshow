import React, { Component } from 'react'
import { connect } from "react-redux"
import { Link } from 'react-router-dom'
import getTheaterAndShowDetail from './actionGetTheaterAndShowDetails'

class TheaterPage extends Component {
    constructor(props) {
        super(props)

        this.state = {
            theaterList: [{
                id: 1, name: "Abhinay Theatre 4K A/C: Gandhinagar", showTimings: ["10:00 AM", "2:30 PM"]
            },
            {
                id: 2, name: "Cinepolis: Forum Shantiniketan, Bengaluru", showTimings: ["10:00 AM", "2:30 PM", "7:00 PM", "10:00 PM"]
            }, {
                id: 3, name: "PVR: MSR Elements Mall, Tanisandhra Main Road", showTimings: ["10:00 AM", "2:30 PM", "7:00 PM", "10:00 PM"]
            }, {
                id: 4, name: "PVR: The Forum Mall, Koramangala", showTimings: ["10:00 AM", "2:30 PM", "7:00 PM", "10:00 PM"]
            },
            {
                id: 5, name: "PVR: VR Bengaluru, Whitefield Road", showTimings: ["10:00 AM", "2:30 PM", "7:00 PM", "10:00 PM"]
            },
            {
                id: 6, name: "Gopalan Mall: Sirsi Circle", showTimings: ["10:00 AM", "2:30 PM", "7:00 PM", "10:00 PM"]
            },
            {
                id: 7, name: "INOX: Central, JP Nagar, Mantri Junction", showTimings: ["10:00 AM", "2:30 PM", "7:00 PM", "10:00 PM"]
            }
            ]

        }
    }
    render() {
        let movieData = this.props.local_variable.movieDetail

        return (

            <div className='d-flex flex-column align-items-center'>
                <div className='header-theater d-flex flex-row justify-content-around align-items-end w-100' >
                    <div>
                        <h3>{movieData.title}</h3>
                        <div className='d-flex flex-row'>
                            <p>{movieData.release_date}</p>
                            <p className='ms-3'>{movieData.runtime} mins</p>
                        </div>
                    </div>
                    <div className='d-flex flex-column justify-content-start'>
                        <h3>Cast & crew</h3>
                        <div className='d-flex flex-row justify-content-start'>
                            <div className="rounded-circle cast bg-secondary"></div>
                            <div className="rounded-circle cast bg-secondary"></div>
                            <div className="rounded-circle cast bg-secondary"></div>
                            <div className="rounded-circle cast bg-secondary"></div>
                            <div className="rounded-circle cast bg-secondary"></div>
                        </div>
                    </div>
                </div>
                <div className='bottom-container bg-light w-75 '>
                    <div>
                        <div className='d-flex flex-row justify-content-end align-items-center mt-2'>
                            <p > <i className="fa-solid text-success fill  fa-circle me-1 ms-2"></i>AVAILABLE</p>
                            <p> <i className="fa-solid text-danger fill fa-circle me-1 ms-2"></i>FAST FILLING</p>
                            <p>  <i className="fa-solid text-success fill fa-language me-1 ms-2"></i>SUBTITILES LANGUAGE </p>
                        </div>
                        <hr className='text-secondary' />



                        {(this.state.theaterList.map(eachTheater => {
                            return (
                                <div className='d-flex container flex-row justify-content-start align-items-center border-2'>
                                    <div className='row'>
                                        <p className='col-1'>  <i className="fa-solid fa-heart text-secondary me-5 align-items-center mt-1  ms-1 "></i></p>
                                        <div className='col-5'>
                                            <p>{eachTheater.name}</p>
                                            <div className='d-flex flex-row'>
                                                <p className='text-success '><i className="fa-solid fa-mobile-screen"></i> M-Ticket</p>
                                                <p className='food ms-3'><i class="fa-solid fa-bowl-food food"></i>Food & Beverage</p>

                                            </div>
                                        </div>
                                        <p className='col-1'><i class="fa-solid fa-shield text-primary"></i>  Info</p>
                                        <div className='d-flex flex-row col-5'>
                                            {
                                                eachTheater.showTimings.map(showTime => {
                                                    return (
                                                        <div>
                                                            <button className='btn text-success border-secondary bg-light ms-1 mt-2' type="button" data-bs-toggle="modal" data-bs-target="#staticBackdrop3" name={eachTheater.name} onClick={this.props.getTheaterAndShowDetail} value={showTime} > {showTime}</button>
                                                            <div className="modal fade modal-3" id="staticBackdrop3" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                                                <div className="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                                                                    <div className="modal-content">
                                                                        <div className="modal-header">
                                                                            <h5 className="modal-title  ms-auto me-auto" id="staticBackdropLabel">Terms & Conditions</h5>
                                                                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                        </div>
                                                                        <div className="modal-body">
                                                                            Pre-booked Food & Beverages have to be collected by the patron from the F&B counter by showing the Booking ID. <br />
                                                                            1. As per the Karnataka Govt Order, Consumption of Food & Beverages inside the Auditorium is Prohibited. <br />
                                                                            2. Patrons above 18 years of age, who are fully vaccinated (both doses) with Covid-19 vaccine will be allowed entry in the cinema premises. <br />
                                                                            3. Unvaccinated individuals will not be admitted and no ticket refund will be provided. <br />
                                                                            4. Cinema staff may check your vaccination status on Aarogya Setu App/Vaccine certificate before allowing entry inside the premises. <br />
                                                                            5. For safety purposes, wearing mask is mandatory. <br />
                                                                            6. Patrons with fever (body temperature more than  99.2 F) or other symptoms like : cough etc. will not be allowed entry and are requested to stay at home. <br />
                                                                            7. Ticket is compulsory for children of 3 years & above. <br />
                                                                            8. Incase the ticket is lost or misplaced, duplicate ticket will not be issued. <br />
                                                                            9. Outside food & beverages are not allowed inside the cinema premises. <br />
                                                                            10. Decision(s) taken by Cinepolis management is final & abiding. <br />
                                                                            11. For 3D movies, ticket price includes charges towards usage of 3D glasses. <br />
                                                                            12. Patrons under the age of 18 years will not be allowed to watch the `A` rated movie.
                                                                        </div>
                                                                        <div className="modal-footer">
                                                                            <button type="button" className="btn bg-light border-danger" data-bs-dismiss="modal">Cancel</button>
                                                                            <Link to="/ticketpage" ><button type="button" className="btn sign-in-bg" data-bs-dismiss="modal">Accept</button> </Link>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>)

                                                })

                                            }


                                        </div>
                                        <hr className='text-secondary' />


                                    </div>
                                </div>
                            )

                        }))}


                    </div>

                </div>

            </div>

        )
    }
}

const mapStateToProps = (state) => ({
    local_variable: state

})


export default connect(mapStateToProps, { getTheaterAndShowDetail })(TheaterPage);
import React, { Component } from 'react'
import getMobileNumAndValidate from "./actionGetMobileNumberAndValidate"
import getEmailAndValidate from "./actionGetEmailAndValidate"
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import validator from "validator"

class PaymentPage extends Component {
    constructor(props) {
        super(props)

        this.state = {
            isVerified: false,
            isPayment: false,
            isOpened: false,
            cardNum: "",
            userName: "",
            expiryMonth: "",
            expiryYear: "",
            cvv: "",
            paymentStatus: false,
            isCardValid: false,
            isNameValid: false,
            isCvvValid: false,
        }
    }

    getUserInput = (event) => {
        let name = event.target.name
        console.log(name)
        let value = event.target.value
        this.setState({ [name]: value })
    }


    validateInput = () => {
        let { cardNum, userName, expiryMonth, cvv } = this.state

        let isCardValid = validator.isCreditCard(cardNum)
        let isNameValid = validator.isAlpha(userName)
        let isCvvValid = validator.isNumeric(cvv) && (cvv.length === 3)
        this.setState({
            isCardValid: true, isNameValid: true, isCvvValid: true
        })


        if (isCardValid && isNameValid && isCvvValid) {
            this.setState({ paymentStatus: true })
        }


    }


    showOrHidePaymentOptions = () => {
        this.setState(prevState => ({
            isOpened: !prevState.isOpened
        }))

    }


    printSeatNumbers = (selectedSeatsArray) => {

        let seatString = ""

        selectedSeatsArray.map(eachSeat => {
            return seatString = eachSeat + "," + seatString
        })

        return seatString

    }
    hideorShowLogin = () => {
        this.setState({ isVerified: true })
    }


    render() {
        // console.log(this.props.local_variable)
        console.log(this.state)
        let mobNumber = this.props.local_variable.mobNumberDetails.mobile
        let isValid = this.props.local_variable.mobNumberDetails.isValid
        // console.log(this.props.local_variable)
        let email = this.props.local_variable.emailDetails.email
        let isValidEmail = this.props.local_variable.emailDetails.isValidEmail
        let data = this.props.local_variable
        let numberOfSeats = parseFloat(this.props.local_variable.numberOfSeats)
        let selectedSeatsArray = this.props.local_variable.seat_number
        let pricePerTicket = this.props.local_variable.pricePerTicket

        let ticketPriceOnly = numberOfSeats * pricePerTicket
        let convenienceFee = ((numberOfSeats * pricePerTicket * 12 / 100) + ((numberOfSeats * pricePerTicket * 12 / 100) * 18 / 100)).toFixed(2)
        let subTotal = ticketPriceOnly + parseFloat(convenienceFee)

        let totalAmountWithBAS = subTotal + numberOfSeats

        let removeText = this.state.bookASmile ? "Remove" : "Add"

        let BASamount = this.state.bookASmile ?

            `Rs.${numberOfSeats * 1}` : `+${numberOfSeats * 1}`

        let basClass = this.state.bookASmile ? "black" : "grey"

        let totalAmount = this.state.bookASmile ? totalAmountWithBAS : subTotal
        let { isOpened } = this.state

        let paymentClass = (isOpened) ? "sign-in-bg" : "payment-page-dd"

        // console.log(data)


        let quickPay = (<div className='QuickPay'>
            <h5>Pay using QuikPay</h5>
            <h6>Credit/ Debit Cards</h6>
            <p>You don't have any cards added in your QuikPay!</p>
            <h6>Net Banking</h6>
            <p>You don't have any NetBanking options added in your QuikPay!
            </p><h6>Redeem Points - BANKING PARTNERS</h6>
            <p>You don't have any redeem points added in your QuikPay!
            </p><h6>Redeem Points - OTHER PARTNERS</h6>
            <p>You don't have any redeem points added in your QuikPay!</p><h6>Other Wallets</h6>
            <p>You don't have any Other Wallet options added in your QuikPay!</p><h6>UPI</h6>
            <p>You don't have any UPI options added in your QuikPay!</p>
            <p>By clicking "Make Payment" you agree to the terms and conditions</p>

        </div>)
        let cardPaymentForm = (<div className='w-100'>
            <h5 className='fs-6 fw-normal text-center'>Enter your Card details</h5>
            <form className='container w-100 ps-3 shadow-lg p-3 mb-5 bg-body rounded card-container'>
                <div className='row'>
                    <label className='col-12 ms-3 mb-2' htmlFor="credit-card">Card Number</label>
                    <input type="number" name="cardNum" value={this.state.cardNum} onChange={this.getUserInput} className='col-9 ms-3' id="credit-card" placeholder='Enter Your Card Number' />
                    <input type="text" onChange={this.getUserInput} name="userName" value={this.state.userName} className='col-9 mt-2 ms-3' placeholder='Name on the card' />
                    <div className='d-flex flex-column col-5 '>
                        <label htmlfor="month" className='col-12 ms-3'>Expiry</label>
                        <div>
                            <input type="month" onChange={this.getUserInput} name="expiryMonth" value={this.state.expiryMonth} className='ms-3 col-4' placeholder='MM ' />
                        </div>
                    </div>
                    <div className='d-flex flex-column col-3'>
                        <label htmlFor='cvv' className='col'>CVV</label>
                        <input type="number" onChange={this.getUserInput} className='col' name='cvv' value={this.state.cvv} placeholder='CVV' />
                    </div>
                </div>

                <input type="checkbox" checked></input>
                <label htmlFor='checkbox'> <img src="./quickPay.png" alt="quickPay" /> QuikPay
                    Save this card information to my BookMyShow account and make faster payments</label>
                <Link to="/successpage" > <button className="continue-btn p-2 mt-5" onClick={this.validateForm}>MAKE PAYMENT</button></Link>
            </form>
        </div>)

        return (
            <div className='payment-page-bg'>
                <div className='container d-flex justify-content-center'>
                    <div className='row'>
                        <div className='col-7 col-md-5'>
                            <div className='container'>
                                <div className='row'>
                                    {this.state.isVerified ? (<div className='col-12 send-tickets mt-1 mb-1'>
                                        <h5 className='p-2 send-tickets fs-6 fw-light w-100'>{`Send tickets to ${email}/ +91 ${mobNumber}`} </h5>
                                    </div>) : (

                                        <div className='col-12'>
                                            <h5 className='p-2 sign-in-bg w-100 fs-6 fw-light'> <i class="fa-solid fa-angle-down me-2 ms-2"></i> Share your Contact Details</h5>
                                            <div className='bg-white col-12 p-3'>
                                                <input type="text" value={email} className="w-100 mb-2 " style={{ "height": "2.2rem" }} placeholder="Email" onChange={this.props.getEmailAndValidate}></input>
                                                {!isValidEmail && email ? <p className="text-danger"> Please Enter Valid Email !!!!</p> : null}

                                                <input type="text" value={mobNumber} style={{ "height": "2.2rem" }} className="w-100 mt-2" placeholder="Mobile number" onChange={this.props.getMobileNumAndValidate}></input>
                                                {!isValid && mobNumber ? <p className="text-danger"> Please Enter Valid Mobile Number !!!!</p> : null}
                                                <button disabled={!isValid} onClick={this.hideorShowLogin} className="continue-btn p-2 mt-5">Continue</button>
                                                <div>
                                                </div>
                                            </div>
                                        </div>)
                                    }
                                    <div className='col-12'>
                                        <p className='payment-page-dd p-3 fs-6 fw-light w-100'><i class="fa-solid fa-angle-down me-2 ms-2"></i>Unlock offers or Apply Promocodes</p>
                                    </div>



                                    <div className='col-12' onClick={this.showOrHidePaymentOptions}>
                                        <p className={`${paymentClass} p-3 col-12 fs-6 fw-light w-100'`}><i class="fa-solid fa-angle-down me-2 ms-2"></i>More Payment options</p>
                                    </div>

                                    {isOpened ? cardPaymentForm : null}

                                    {
                                        // <div class="dropdown">
                                        //     <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-bs-toggle="dropdown" aria-expanded="false">
                                        //         Dropdown
                                        //     </button>
                                        //     <div class="dropdown-menu">
                                        //         <div>
                                        //             <p>QuickPay</p>
                                        //             <p>Credit/ Debit Card</p>
                                        //             <p>Net Banking</p>
                                        //             <p>Mobile Wallets</p>
                                        //             <p>Gift Voucher</p>
                                        //             <p>UPI</p>
                                        //             <p>Redeem Points</p>
                                        //         </div>

                                        //     </div>
                                        // </div> 
                                    }

                                    <div className='col-12 p-3 payment-page-dd fs-6 fw-light'>
                                        <div className=' fs-6 fw-light'>
                                            <input type="checkbox" id="checkbox" />
                                            <label htmlFor='checkbox' className='ms-2'>Earn Loyalty points</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className='col-6 col-lg-4 p-2 bg-white text-left'>
                            <div className='container'>
                                <div className='row'>
                                    <h5 className='text-dark fw-light col-12 fs-5'>ORDER SUMMARY</h5>
                                    <div className='col-12 d-flex justify-content-between p-2'>
                                        <h5 className='fw-normal mt-2'>{data.movieDetail.title}</h5>
                                        <h5>{numberOfSeats}<br /> Tickets</h5>
                                    </div>
                                    <p className='text-secondary'>English,2D</p>
                                    <p className='text-secondary'>{data.theaterAndshowDetails.theaterName} </p>
                                    <p className='text-secondary'>M-Ticket</p>
                                    <p>Seat Numbers- {this.printSeatNumbers(selectedSeatsArray)}</p>
                                    <p>Show time- {data.theaterAndshowDetails.showTime}</p>
                                    <hr className='text-secondary' />
                                    <div className='col-12 d-flex justify-content-between'>
                                        <p className='fs-6 text-secondary'>Sub total </p>
                                        <p className='fs-5'>Rs.{subTotal}</p>
                                    </div>
                                    <div className='col-12 d-flex justify-content-between'>
                                        <p className='fs-6 text-secondary'>+Convenience fees </p>
                                        <p className='fs-5'>Rs.{convenienceFee}</p>
                                    </div>

                                    <div className='bas-container container round-custom-big col-11 p-2'>
                                        <div className='row'>
                                            <img src="./bookASmile.png" alt="bookasmile" className="BAS col-3" />
                                            <div className='col-6'>
                                                <h6 className={`${basClass}`}>Contribution to BookASmile</h6>

                                                <p>(Rs.1 per ticket has been added)</p>
                                                <p>VIEW T&C</p>
                                            </div>
                                            <div className="col-2">
                                                <p className={`${basClass} `}> <b>{BASamount}</b></p>
                                                <p className='text-danger' onClick={this.removeBookASmile}>{removeText}</p>
                                            </div>

                                        </div>
                                    </div>
                                    <div className='col-12 mt-3 d-flex justify-content-between  align-content-center amt-payable pt-1 pb-1'>
                                        <p className='fs-6 ps-2'>Amount Payable </p>
                                        <p className='fs-6 pe-2'>Rs.{totalAmount}</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className='footer col-12'>
                            <p>Note:</p>
                            <p>1.You can cancel the tickets 20 min(s) before the show. Refunds will be done according to <span className='text-danger'>Cancellation Policy.</span></p>
                            <p>2.    In case of Credit/Debit Card bookings, the Credit/Debit Card and Card holder must be present at the ticket counter while collecting the ticket(s).
                            </p>
                            <div className='d-flex justify-content-between align-items-center'>

                                <p>Bigtree Entertainment Pvt. Ltd. <span className='text-dark' >Privacy Policy | Contact Us</span>
                                </p>
                                <div className='d-flex align-items-center'>
                                    <p className='fs-6'>As safe as it gets</p>
                                    <img src="./SISA.png" alt="payment-logo" className='ms-2' />
                                    <img src="./Entrust.png" alt="payment-logo" className='ms-2' />
                                    <img src="./masterCard.png" alt="payment-logo" className='ms-2' />
                                    <img src="./safeKey.png" alt="payment-logo" className='ms-2' />
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>



        )
    }
}

let mapStateToProps = (state) => ({
    local_variable: state
})
export default connect(mapStateToProps, { getMobileNumAndValidate, getEmailAndValidate })(PaymentPage)
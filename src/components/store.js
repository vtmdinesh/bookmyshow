import { createStore, applyMiddleware } from "redux";
import allReducers from "../reducers/combinedReducer";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";

const middleware = [thunk]

const store = createStore(allReducers,
    composeWithDevTools(applyMiddleware(...middleware)));

export default store; 
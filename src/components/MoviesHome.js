import React, { Component } from 'react'
import RecommandedMovies from './RecommandedMovies'
import PopularMovies from './PopularMovies'
import { Link } from 'react-router-dom'
import Carousel from "./Carousel"


class MoviesHome extends Component {
    render() {
        return (
            <div>
                <Carousel />
                <div className='d-flex flex-column w-100 justify-content-center align-items-center'>

                    <div className='movies-home'>
                        <div className='d-flex flex-row justify-content-between mt-3'>
                            <h3 className='text-left'>Recommanded Movies</h3>
                            <Link to="/recommandedMovies" className='see-all-text'>See all  <i class="fa-solid fa-arrow-right"></i></Link>
                        </div>
                        <RecommandedMovies />
                        <img src="https://assets-in.bmscdn.com/discovery-catalog/collections/tr:w-1440,h-120:q-80/lead-in-v3-collection-202102040828.png" alt="offer" className='w-100 mt-5 mb-5' />
                        <div className='d-flex flex-row justify-content-between mt-3'>
                            <h3 className='text-left'>Popular Movies</h3>
                            <Link to="/popularMovies" className='see-all-text'>See all <i class="fa-solid fa-arrow-right"></i></Link>
                        </div>
                        <PopularMovies />


                    </div></div>
            </div>
        )
    }
}



export default MoviesHome
import React, { Component } from 'react'
import { connect } from 'react-redux'
import TableRow from './TableRow';
import CoupleRow from './CoupleRow';
import getSeatNumbers from './actionGetSeatNumbers';
import getPaymentdetails from './actionGetPayementDetails ';
import { Link } from 'react-router-dom';

class TheatreSeatingLayout extends Component {

    constructor(props) {
        super(props)

        this.state = {
            selectedSeats: [],
            isChosen: false,
            snacks: [],
            bookASmile: true
        }
    }

    seatSelection = () => {

        this.setState({ isChosen: true })

    }

    getSeatCategory = (pricePerTicket) => {
        switch (pricePerTicket) {
            case 120:
                return "CLASSIC";
            case 200:
                return "PRIME";
            case 340:
                return "VIP";
            case 500:
                return "COUPLE"
            default:
                break;
        }
    }

    printSeatNumbers = (selectedSeatsArray) => {

        let seatString = ""

        selectedSeatsArray.map(eachSeat => {
            return seatString = eachSeat + "," + seatString
        })

        return seatString

    }
    removeBookASmile = () => {
        this.setState(prevState => ({
            bookASmile: !prevState.bookASmile
        }))
    }

    render() {

        // console.log(this.props.local_variable)
        let data = this.props.local_variable
        let numberOfSeats = parseFloat(this.props.local_variable.numberOfSeats)
        let selectedSeatsArray = this.props.local_variable.seat_number
        let pricePerTicket = this.props.local_variable.pricePerTicket

        let ticketPriceOnly = numberOfSeats * pricePerTicket
        let convenienceFee = ((numberOfSeats * pricePerTicket * 12 / 100) + ((numberOfSeats * pricePerTicket * 12 / 100) * 18 / 100)).toFixed(2)
        let subTotal = ticketPriceOnly + parseFloat(convenienceFee)

        let totalAmountWithBAS = subTotal + numberOfSeats

        let removeText = this.state.bookASmile ? "Remove" : "Add"

        let BASamount = this.state.bookASmile ?

            `Rs.${numberOfSeats * 1}.00` : `+Rs.${numberOfSeats * 1}.00`

        let basClass = this.state.bookASmile ? "black" : "grey"

        let totalAmount = this.state.bookASmile ? totalAmountWithBAS : subTotal


        let seatLayout = (
            <div className='row'>
                <div className='container-fluid col bg-light'>
                    <div className='row d-flex flex-column align-items-center'>

                        <div className='col layout-header d-flex flex-row justify-content-between'>
                            <div className='d-flex flex-row align-items-center'>
                                <Link to="/theaterpage" ><i class="fa-solid fa-less-than fs-1 me-5 ms-2"></i></Link>
                                <div>
                                    <h3>{data.movieDetail.title}</h3>
                                    <div>
                                        <p>{data.theaterAndshowDetails.theaterName}|{data.theaterAndshowDetails.showTime}</p>

                                    </div>
                                </div>
                            </div>
                            <div className='d-flex flex-row align-items-center'>
                                <button className='text-white p-2 round-custom bg-transparent border-white me-5 '>{data.numberOfSeats} Tickets  <Link to="/ticketpage"> <i class="fa-solid ms-2 fa-pencil"></i> </Link></button>

                                <Link to="/theaterpage"> <i class="fa-solid fa-xmark fs-4 me-5 ms-3"></i> </Link>
                            </div>


                        </div>

                        <div className='container-fluid col layout bg-white d-flex flex-column bg-light align-items-center'>

                            <div className='row w-100 overflow-auto d-flex flex-column justify-content-center'>
                                <div className='col'>

                                    <h3 className='text-secondary fs-6 mt-2'> VIP-Rs. 340.00 </h3>
                                    <hr className='text-secondary' />

                                </div>


                                <div className='col container-fluid'>
                                    <div className='row'>
                                        <TableRow rowId="A" />
                                    </div>
                                </div>
                                <div className='col'
                                >
                                    <h3 className='text-secondary fs-6'> Prime-Rs. 200.00 </h3>
                                    <hr className='text-secondary' />

                                </div>
                                <div className='col container-fluid'>
                                    <div className='row'>
                                        <TableRow rowId="B" />
                                        <TableRow rowId="C" />
                                        <TableRow rowId="D" />
                                        <TableRow rowId="E" />
                                    </div>
                                </div>
                                <div className='col'>

                                    <h3 className='text-secondary fs-6'> Couple Seats-Rs. 500.00 </h3>
                                    <hr className='text-secondary' />

                                </div>


                                <div className='col-12'>

                                    <CoupleRow rowId="F" />
                                    <CoupleRow rowId="G" />
                                    <CoupleRow rowId="H" />

                                </div>
                                <div className='col'>

                                    <h3 className='text-secondary fs-6'> CLASSIC-Rs.120.00 </h3>
                                    <hr className='text-secondary ' />
                                </div>

                                <div className='col'>

                                    <TableRow rowId="I" />
                                    <TableRow rowId="J" />


                                </div>

                            </div>

                            <img src="./screen.png" alt="screen" className='w-25 mt-5 align-self-center' />
                            <p className='mt-3 align-self-center'>All eyes this way please !</p>

                        </div>

                        {
                            (selectedSeatsArray.length === numberOfSeats) ? <div className='col bg-light pt-3 pb-3 w-100 text-center'>
                                <button id="continue-btn" onClick={this.seatSelection} className="sign-in-bg border-0 w-50 p-2 rounded-3">Pay Rs.{numberOfSeats * pricePerTicket}.00</button>
                            </div> : null


                        }

                    </div>

                </div>
            </div>)


        let summaryPage = (
            <div className='row d-flex justify-content-center payment-page-bg'>
                <img src="./img.png" className='col-6 col-lg-6' alt="page" />
                <div className='col-6 col-lg-3 p-3 bg-white'>
                    <div className='container'>
                        <div className='row'>
                            <h5 className='text-danger fw-light col-12 fs-5'>BOOKING SUMMARY</h5>
                            <div className='col d-flex justify-content-between'>
                                <p>
                                    {this.getSeatCategory(pricePerTicket)} <span> {this.printSeatNumbers(selectedSeatsArray)}</span><span>( {numberOfSeats} Tickets)</span></p>
                                <p> Rs. {ticketPriceOnly}.00</p>

                            </div>
                            <div className='col-12 d-flex justify-content-between'>
                                <p>convenience fees</p>
                                <p>Rs.{convenienceFee}</p>
                            </div>
                            <hr className='text-secondary' />
                            <div className='col-12 d-flex justify-content-between'>
                                <p className='fs-5'>Sub total </p>
                                <p className='fs-5'>Rs.{subTotal}</p>
                            </div>

                            <div className='bas-container container round-custom-big col-11 p-2'>
                                <div className='row'>
                                    <img src="./bookASmile.png" alt="bookasmile" className="BAS col-3" />
                                    <div className='col-6'>
                                        <h6 className={`${basClass}`}>Contribution to BookASmile</h6>

                                        <p>(Rs.1 per ticket has been added)</p>
                                        <p>VIEW T&C</p>
                                    </div>
                                    <div className="col-2">
                                        <p className={`${basClass} `}> <b>{BASamount}</b></p>
                                        <p className='text-danger' onClick={this.removeBookASmile}>{removeText}</p>
                                    </div>

                                </div>
                            </div>


                            <p className='mt-2 '>Your current state is <span className='text-danger'>Karnataka</span></p>
                            <div className='col-12 d-flex justify-content-between amt-payable pt-1 pb-1'>
                                <p className='fs-5 ps-2'>Amount Payable </p>
                                <p className='fs-5 pe-2'>Rs.{totalAmount}</p>
                            </div>


                            <p className='text-center text-secondary fw-normal mt-2 fs-5'>SELECT TICKET TYPE</p>
                            <div className='text-center ms-5 bg-white border d-flex align-items-center justify-content-start border-secondary rounded-3 w-75'>
                                <i class="fa-solid text-danger fa-circle-dot fs-4 p-3 border-end  "></i>
                                <p className='fs-4 mt-2 ms-2'><i class="fa-solid fa-comment-sms"></i> <sup className='ms-2 text-danger fs-6'>M-Ticket</sup></p>
                            </div>
                            <p className='col-12 text-center mt-2 fs-6'>Show the m-ticket QR Code on your mobile to enter the cinema.</p>
                            <p className='text-secondary text-center'> <i class="fa-solid text-secondary fa-circle-exclamation me-2"></i>By proceeding, I express my consent to complete this transaction.</p>
                            <Link to="/paymentpage"> <button className='sign-in-bg text-center p-3 border-0 w-100 rounded-3'>TOTAL: Rs. {totalAmount}                        <span className='ms-auto'>Proceed</span></button> </Link>
                            <p className='text-center text-secondary mt-2'> You can cancel the tickets 20 min(s) before the show. Refunds will be done according to <span className='text-danger'>Cancellation Policy</span> </p>

                        </div>
                    </div>
                </div>
            </div>
        )
        return (
            <div className='container-fluid'>
                {this.state.isChosen ? summaryPage : seatLayout}
            </div>
        )
    }
}

let mapStateToProps = state => ({
    local_variable: state
})


export default connect(mapStateToProps, { getSeatNumbers, getPaymentdetails })(TheatreSeatingLayout)
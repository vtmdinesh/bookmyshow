import React, { Component } from 'react'
import { connect } from "react-redux"
import getSeatNumbers from './actionGetSeatNumbers'


class CoupleRow extends Component {

    selectSeat = (event) => {
        let id = event.target.id
        let seat_number = this.props.local_variable.seat_number
        let numberOfSeats = this.props.local_variable.numberOfSeats

        if (numberOfSeats > seat_number.length) {
            event.target.classList.toggle("selected")
            if (seat_number.length >= 0) {
                let newArray = seat_number.filter((eachId) => eachId !== id)

                if (newArray.length === seat_number.length && newArray.length < numberOfSeats) {
                    console.log("in if")
                    this.props.getSeatNumbers([...newArray, id])
                }
                else {
                    console.log("in else")
                    this.props.getSeatNumbers([...newArray])
                }
            }
        }
        else if (numberOfSeats == seat_number.length) {

            let newArray = seat_number.filter((eachId) => eachId !== id)
            if (newArray.length !== seat_number.length) {
                event.target.classList.toggle("selected")
                this.props.getSeatNumbers([...newArray])
            }
        }
    }


    render() {
        let rowId = this.props.rowId
        // console.log(this.props)
        return (
            <div className='col overflow-smooth d-flex justify-content-center'>

                <td colSpan="2"><div className='text-secondary fs-6'>{rowId}</div></td>
                <td colSpan="2"><div className='empty-box'></div></td>
                <td colSpan="2"><div className='empty-box'></div></td>
                <td colSpan="2"><div className='couple-box text-center d-flex flex-row justify-content-center align-items-center ' >
                    <div className='text-center couple-left-box' id={`${rowId}-1`} onClick={this.selectSeat} >1</div>
                    <div className='text-center couple-right-box' id={`${rowId}-2`} onClick={this.selectSeat} >2</div>
                </div></td>
                <td colSpan="2"><div className='couple-box text-center d-flex flex-row justify-content-center align-items-center' >
                    <div className='text-center couple-left-box' id={`${rowId}-3`} value="2" onClick={this.selectSeat} >3</div>
                    <div className='text-center couple-right-box' id={`${rowId}-4`} value="2" onClick={this.selectSeat} >4</div>
                </div></td>
                <td colSpan="2"><div className='couple-box text-center d-flex flex-row justify-content-center align-items-center' >
                    <div className='text-center couple-left-box' id={`${rowId}-5`} value="2" onClick={this.selectSeat} >5</div>
                    <div className='text-center couple-right-box' id={`${rowId}-6`} onClick={this.selectSeat} >6</div>
                </div></td>


                <td colSpan="2"><div className='empty-box'></div></td>
                <td colSpan="2"><div className='empty-box'></div></td>
                <td colSpan="2"><div className='couple-box text-center d-flex flex-row justify-content-center align-items-center ' >
                    <div className='text-center couple-left-box' id={`${rowId}-7`} onClick={this.selectSeat} >7</div>
                    <div className='text-center couple-right-box' id={`${rowId}-8`} onClick={this.selectSeat} >8</div>
                </div></td>
                <td colSpan="2"><div className='couple-box text-center d-flex flex-row justify-content-center align-items-center' >
                    <div className='text-center couple-left-box' id={`${rowId}-9`} onClick={this.selectSeat} >9</div>
                    <div className='text-center couple-right-box' id={`${rowId}-10`} onClick={this.selectSeat} >10</div>
                </div></td>
                <td colSpan="2"><div className='couple-box text-center d-flex flex-row justify-content-center align-items-center ' >
                    <div className='text-center couple-left-box' id={`${rowId}-11`} onClick={this.selectSeat} >11</div>
                    <div className='text-center couple-right-box' id={`${rowId}-121`} onClick={this.selectSeat} >12</div>
                </div></td>




                <td colSpan="2"><div className='empty-box'></div></td>
                <td colSpan="2"><div className='empty-box'></div></td>
                <td colSpan="2"><div className='couple-box text-center d-flex flex-row justify-content-center align-items-center ' >
                    <div className='text-center couple-left-box' id={`${rowId}-13`} onClick={this.selectSeat}>13</div>
                    <div className='text-center couple-right-box' id={`${rowId}-14`} onClick={this.selectSeat}>14</div>
                </div></td>
                <td colSpan="2"><div className='couple-box text-center d-flex flex-row justify-content-center align-items-center ' >
                    <div className='text-center couple-left-box' id={`${rowId}-15`} onClick={this.selectSeat}>15</div>
                    <div className='text-center couple-right-box' id={`${rowId}-16`} onClick={this.selectSeat}>16</div>
                </div></td>
                <td colSpan="2"><div className='couple-box text-center d-flex flex-row justify-content-center align-items-center ' >
                    <div className='text-center couple-left-box' id={`${rowId}-19`} onClick={this.selectSeat}>19</div>
                    <div className='text-center couple-right-box' id={`${rowId}-20`} onClick={this.selectSeat}>20</div>
                </div></td>





            </div>
        )

    }
}


let mapStateToProps = (state) => ({
    local_variable: state
})

export default connect(mapStateToProps, { getSeatNumbers })(CoupleRow)
import { FETCH_RECOMMANDED_MOVIES } from "./action-types"
import axios from "axios"

const getMoviesList = () => dispatch => {

    axios.get("https://api.themoviedb.org/3/movie/now_playing?api_key=c88885ed1df6c2b1eec648f6a4cc3707&language=en-US&page=1")
        .then(res => res.data.results)
        .then(data => dispatch({
            type: FETCH_RECOMMANDED_MOVIES,
            payload: data
        }))
};


export default getMoviesList;

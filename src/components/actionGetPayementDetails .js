import { GET_PAYMENT_DETAILS } from "./action-types"


const getPaymentDetails = (obj) => dispatch => {

    dispatch({
        type: GET_PAYMENT_DETAILS,
        payload: obj
    })
};


export default getPaymentDetails;
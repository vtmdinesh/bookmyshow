import React, { Component } from 'react'
import { connect } from 'react-redux';
import { v4 as uuidv4 } from 'uuid';
class SuccessPage extends Component {
    render() {

        let movieDetails = this.props.local_variable.movieDetail
        let theatreDetails = this.props.local_variable.theatreDetails
        console.log(theatreDetails)
        let posterPath = movieDetails.poster_path
        let title = movieDetails.title
        // console.log(movieDetails)
        let data = this.props.local_variable
        let numberOfSeats = parseFloat(this.props.local_variable.numberOfSeats)
        let selectedSeatsArray = this.props.local_variable.seat_number
        let pricePerTicket = this.props.local_variable.pricePerTicket
        let ticketPriceOnly = numberOfSeats * pricePerTicket
        let convenienceFee = ((numberOfSeats * pricePerTicket * 12 / 100) + ((numberOfSeats * pricePerTicket * 12 / 100) * 18 / 100)).toFixed(2)
        let subTotal = ticketPriceOnly + parseFloat(convenienceFee)




        return (
            <div className='d-flex flex-column align-items-center bg-white payment-page-bg'>
                <img src="./images/logo.png" className='w-25 h25' alt="logo" />
                <h5 className='text-success'>Your booking is confirmed!</h5>
                <p>Booking ID <b> {uuidv4()}</b></p>
                <div className='bg-light'>
                </div>
                <div className='payment-page-bg w-75'>
                    <div className='d-flex justify-content-between'>
                        <img src={`https://image.tmdb.org/t/p/original/${movieDetails.poster_path}`} alt="poster" className='poster' />
                        <div className='d-flex flex-column me-auto'
                        >
                            <h1>{title}</h1>{

                                // <p></p>
                                // <p>Ahamadabad chinnai adjfsdj</p>
                                // <br />
                                // <h4>Gold -C3,C9,C0</h4>
                            }
                        </div>
                        <img src="./QR.jpg" className='w-25'></img>
                    </div>
                    <div className='d-flex justify-content-between mt-4 align-items-center'>
                        <div className='d-flex flex-column mt-2 text-center'>
                            <h1>{numberOfSeats}</h1>
                            <p>Tickets</p>
                        </div>
                        <img src="./booking.jpg" className='w-25'></img>
                    </div>
                </div>


            </div>
        )
    }
}

let mapStateToProps = (state) => ({
    local_variable: state
})

export default connect(mapStateToProps, null)(SuccessPage)
import React from 'react'
import { Link } from 'react-router-dom'
import { connect } from "react-redux"
import { getId } from "./actionGetMovieId"


let MovieTile = (props) => {

    let { poster_path, title, vote_average, vote_count, id } = props.movieData
    return (
        <Link className='movie-tile col-md-3' onClick={props.getId} to={`/movie/${id}`} style={{ width: "25%", display: 'flex', justifyContent: 'center' }} >
            <div className="m-2 w-100 tile" id={id}>
                <div className="w-100 rounded-3">
                    <img id={id} src={`https://image.tmdb.org/t/p/original/${poster_path}`} alt="movie_poster" className="w-100  image radius" />
                    <div id={id} className='d-flex flex-row bg-dark w-100 text-white radius-2 justify-content-center pt-2'>
                        <i class="fa-solid fa-heart bg-danger text-danger align-items-center mt-1  ms-1 bg-dark"></i>
                        <p className='align-self-center ms-1'>{`${vote_average * 10} %`}</p>
                        <p className='ms-1 ' > {`${vote_count}K votes`}</p>
                    </div>
                </div>
                <h5 id={id} className="tile-title">{title}</h5>
            </div>
        </Link>
    )
}

const mapStateToProps = (state) => ({
    local_variable: state
})

export default connect(mapStateToProps, { getId })(MovieTile)
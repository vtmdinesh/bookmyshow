import { GET_NUMBER_OF_SEATS } from "./action-types"


const getNumberOfSeats = (event) => dispatch => {
    let numberOfSeats = event.target.value
    console.log(numberOfSeats)
    dispatch({
        type: GET_NUMBER_OF_SEATS,
        payload: numberOfSeats
    })
};


export default getNumberOfSeats;
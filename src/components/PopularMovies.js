import React, { Component } from 'react'
import { connect } from 'react-redux'
import MovieTile from './MovieTile'


class PopularMovies extends Component {

    render() {
        let moviesList = this.props.local_variable.popularMovies[0]
        // console.log(moviesList)
        return (
            <div style={{ "flexBasis": "300px" }} className='overflow-auto d-flex flex-row w-100 tile'>

                {moviesList ? <div id="carouselExampleControls-3" class="carousel d-none d-md-block" data-bs-ride="carousel">
                    <div class="carousel-inner transition">
                        <div class="carousel-item active">
                            <div className='d-flex'>
                                <MovieTile movieData={moviesList[0]} key={moviesList[0].id} />
                                <MovieTile movieData={moviesList[1]} key={moviesList[1].id} />
                                <MovieTile movieData={moviesList[2]} key={moviesList[2].id} />
                                <MovieTile movieData={moviesList[3]} key={moviesList[3].id} />

                            </div>
                        </div>
                        <div class="carousel-item">
                            <div className='d-flex'>
                                <MovieTile movieData={moviesList[4]} key={moviesList[4].id} />
                                <MovieTile movieData={moviesList[5]} key={moviesList[5].id} />
                                <MovieTile movieData={moviesList[6]} key={moviesList[6].id} />
                                <MovieTile movieData={moviesList[7]} key={moviesList[7].id} />

                            </div>
                        </div>
                    </div>
                    <button className="carousel-control-prev bt" type="button" data-bs-target="#carouselExampleControls-3" data-bs-slide="prev">
                        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span className="visually-hidden">Previous</span>
                    </button>
                    <button className="carousel-control-next bt" type="button" data-bs-target="#carouselExampleControls-3" data-bs-slide="next">
                        <span className="carousel-control-next-icon" aria-hidden="true"></span>
                        <span className="visually-hidden">Next</span>
                    </button>
                </div> : null}

                {moviesList ? <div className='d-flex d-md-none flex-row overflow-smooth'>



                    {(moviesList.map(eachMovie => {
                        return <MovieTile movieData={eachMovie} key={eachMovie.id}
                        />
                    }))}
                </div>

                    : null
                }




            </div>
        )
    }
}




const mapStateToProps = (state) => ({
    local_variable: state

})

export default connect(mapStateToProps)(PopularMovies)

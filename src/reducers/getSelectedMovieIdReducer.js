import { GET_SELECTED_MOVIE_ID } from "../components/action-types"


const getSelectedIdReducer = (state = [], action) => {
    switch (action.type) {
        case (GET_SELECTED_MOVIE_ID):
            return state = action.payload
        default:
            return state
    }
}

export default getSelectedIdReducer;
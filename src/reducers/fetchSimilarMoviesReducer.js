import { FETCH_SIMILAR_MOVIES } from "../components/action-types"


const fetchSimilarMovieDetailsReducer = (state = [], action) => {
    switch (action.type) {
        case (FETCH_SIMILAR_MOVIES):
            return state = action.payload
        default:
            return state
    }
}

export default fetchSimilarMovieDetailsReducer;
import { FETCH_MOVIE_DETAIL } from "../components/action-types"


const fetchMovieDetailsReducer = (state = [], action) => {
    switch (action.type) {
        case (FETCH_MOVIE_DETAIL):
            return state = action.payload
        default:
            return state
    }
}

export default fetchMovieDetailsReducer;
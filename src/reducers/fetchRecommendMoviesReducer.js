import { FETCH_RECOMMANDED_MOVIES } from "../components/action-types"


const fetchRecommendMoviesReducer = (state = [], action) => {
    switch (action.type) {
        case (FETCH_RECOMMANDED_MOVIES):
            return state = [...state, action.payload]
        default:
            return state
    }
}

export default fetchRecommendMoviesReducer;
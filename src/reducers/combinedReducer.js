import { combineReducers } from "redux";
import fetchPopularMoviesReducer from "./fetchPopularMovies";
import fetchRecommendMoviesReducer from "./fetchRecommendMoviesReducer";
import fetchMovieDetailsReducer from "./fetchMovieDetailReducer";
import fetchSimilarMovieDetailsReducer from "./fetchSimilarMoviesReducer";
import getSelectedIdReducer from "./getSelectedMovieIdReducer"
import getTheaterAndShowDetailsReducer from "./getTheaterAndShowDetailsReducer"
import getNumberOfSeatsReducer from "./getNumberOfSeatsReducer";
import getPricePerTicketReducer from "./getPricePerTicketReducer";
import getSeatNumbersReducer from "./getSeatNumberDetailsReducer";
import getMobileNumAndValidateReducer from "./getMobileNumberAndValidate";
import getEmailAndValidateReducer from "./getEmailAndValidateReducer";
import getPaymentDetailsReducer from "./getPaymentDetailsReducer";


const allReducers = combineReducers({
    recommendMovies: fetchRecommendMoviesReducer,
    popularMovies: fetchPopularMoviesReducer,
    movieDetail: fetchMovieDetailsReducer,
    similarMovies: fetchSimilarMovieDetailsReducer,
    selectedId: getSelectedIdReducer,
    theaterAndshowDetails: getTheaterAndShowDetailsReducer,
    numberOfSeats: getNumberOfSeatsReducer,
    pricePerTicket: getPricePerTicketReducer,
    seat_number: getSeatNumbersReducer,
    mobNumberDetails: getMobileNumAndValidateReducer,
    emailDetails: getEmailAndValidateReducer,
    paymentDetails: getPaymentDetailsReducer

})


export default allReducers
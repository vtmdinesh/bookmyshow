import { GET_THEATER_AND_SHOW_DETAILS } from "../components/action-types"


const getTheaterAndShowDetailsReducer = (state = [], action) => {
    switch (action.type) {
        case (GET_THEATER_AND_SHOW_DETAILS):
            return state = action.payload
        default:
            return state
    }
}

export default getTheaterAndShowDetailsReducer;
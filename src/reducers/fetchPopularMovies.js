import { FETCH_POPULAR_MOVIES } from "../components/action-types"


const fetchPopularMoviesReducer = (state = [], action) => {
    switch (action.type) {
        case (FETCH_POPULAR_MOVIES):
            return state = [...state, action.payload]
        default:
            return state
    }
}

export default fetchPopularMoviesReducer;